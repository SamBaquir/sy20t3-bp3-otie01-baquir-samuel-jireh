#include "Enemy.h"
#include "Scene.h"

Enemy::Enemy()
{
}

Enemy::~Enemy()
{
}

void Enemy::start()
{

	//Load texture
	texture = loadTexture("gfx/enemy.png");

	// Initialize to avoid garbage values
	directionX = -1;
	directionY = 1;
	width = 0;
	height = 0;
	speed = 2;
	reloadTime = 45; // Reload time of 120 frames, or 0.13 seconds
	currentReloadTime = 0;
	directionChangeTime = (rand() % 120) + 180; // Direction change time of 3-5 seconds
	currentDirectionChangeTime = 0;

	// Query the texture to set our width and height
	SDL_QueryTexture(texture, NULL, NULL, &width, &height);

	sound = SoundManager::loadSound("sound/334227__jradcoolness__laser.ogg");
	sound->volume = 64;
}

void Enemy::update()
{
	
	// Move
	x += directionX * speed;
	y += directionY * speed;

	//Switch directions immediately if collides with edge
	if (x < 0 || x > SCREEN_WIDTH - width)
	{
		directionX = -directionX;
		currentDirectionChangeTime = directionChangeTime;
	}

	// Basic AI, switch directions every X seconds
	if (currentDirectionChangeTime > 0)
		currentDirectionChangeTime--;

	if (currentDirectionChangeTime == 0)
	{	
		// Flip directions
		directionX = -directionX;
		currentDirectionChangeTime = directionChangeTime;
	}

	//Decrement the enemy's reload timer
	if (currentReloadTime > 0)
		currentReloadTime--;

	// Only fire when our reload timer is ready
	if (currentReloadTime == 0)
	{
		float dx = -1;
		float dy = -1;

		calcSlope(playerTarget->getPositionX(), playerTarget->getPositionY(), x, y, &dx, &dy);

		SoundManager::playSound(sound);
		Bullet* bullet = new Bullet(x - 3 + width / 2, y + height, dx, dy, 5, Side::ENEMY_SIDE);
		bullets.push_back(bullet);
		getScene()->addGameObject(bullet);

		//After firing, reset our reload timer
		currentReloadTime = reloadTime;
	}

	// Memory manage our bullets. When they go off screen, delete them
	for (int i = 0; i < bullets.size(); i++)
	{
		if (bullets[i]->getPositionY() >= 900 || bullets[i]->getPositionY() <= 0 || bullets[i]->getPositionX() >= SCREEN_WIDTH || bullets[i]->getPositionX() <= 0)
		{
			//Cache the variable so we can delete it later
			Bullet* bulletToErase = bullets[i];
			bullets.erase(bullets.begin() + i);
			delete bulletToErase;

			std::cout << "Bullet destroyed!" << "\n";
			break;
		}
	}

	//Despawn enemy if it is beyond screen height
	

}

void Enemy::draw()
{
	blit(texture, x, y);
}

void Enemy::setPlayerTarget(Player* player)
{
	playerTarget = player;
}

void Enemy::setPosition(int xPos, int yPos)
{
	this->x = xPos;
	this->y = yPos;
}

int Enemy::getPositionX()
{
	return x;
}

int Enemy::getPositionY()
{
	return y;
}

int Enemy::getWidth()
{
	return width;
}

int Enemy::getHeight()
{
	return height;
}
