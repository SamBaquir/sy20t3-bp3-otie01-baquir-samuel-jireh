#include "Player.h"
#include "Scene.h"

Player::~Player()
{
	// Memory manage our bullets. Delete all bullets on player deletion/death
	for (int i = 0; i < bullets.size(); i++)
	{
		delete bullets[i];
	}
	bullets.clear();
}

void Player::start()
{
	//Load texture
	texture = loadTexture("gfx/player.png");

	// Initialize to avoid garbage values
	x = 350;
	y = 700;
	width = 0;
	height = 0;
	speed = 5;
	reloadTime = 13; // Reload time of 26 frames, or 0.26 seconds
	currentReloadTime = 0;
	isAlive = true;
	buffUp = false;

	// Query the texture to set our width and height
	SDL_QueryTexture(texture, NULL, NULL, &width, &height);

	sound = SoundManager::loadSound("sound/334227__jradcoolness__laser.ogg");
}

void Player::update()
{
	//Memory manage our bullets. When they go off screen, delete them
	for (int i = 0; i < bullets.size(); i++)
	{
		if (bullets[i]->getPositionY() <= 0)
		{
			//Cache the variable so we can delete it later
			//We can't delete it after erasing from the vector (leaked pointer)
			Bullet* bulletToErase = bullets[i];
			bullets.erase(bullets.begin() + i);
			delete bulletToErase;

			//We can't mutate (change) our vector while looping inside it
			// this might crash on the next loop iteration
			// To counter that, we only delete one bullet per frame
			// We can also avoid lag this way
			break;

		}
	}

	if (!isAlive) return;

	if (getPositionY() > 0)
	{
		if (app.keyboard[SDL_SCANCODE_W] || app.keyboard[SDL_SCANCODE_UP])
		{

			y -= speed;
		}
	}

	if (getPositionY() < SCREEN_HEIGHT - height)
	{
		if (app.keyboard[SDL_SCANCODE_S] || app.keyboard[SDL_SCANCODE_DOWN])
		{
			y += speed;
		}
	}
	
	if (getPositionX() > 0)
	{
		if (app.keyboard[SDL_SCANCODE_A] || app.keyboard[SDL_SCANCODE_LEFT])
		{
			x -= speed;
		}
	}

	if (getPositionX() < SCREEN_WIDTH - width)
	{
		if (app.keyboard[SDL_SCANCODE_D] || app.keyboard[SDL_SCANCODE_RIGHT])
		{
			x += speed;
		}
	}
	
	//Decrement the player's reload timer
	if (currentReloadTime > 0)
		currentReloadTime--;

	if (!buffUp)
	{
		if ((app.keyboard[SDL_SCANCODE_F] || app.keyboard[SDL_SCANCODE_SPACE]) && currentReloadTime == 0)
		{
			SoundManager::playSound(sound);
			Bullet* bullet = new Bullet(x - 4 + width / 2, y - height / 2, 0, -1, 10, Side::PLAYER_SIDE);
			bullets.push_back(bullet);
			getScene()->addGameObject(bullet);

			//After firing, reset our reload timer
			currentReloadTime = reloadTime;
		}
	}
	else
	{
		poweredUp();
	}
	
}

void Player::draw()
{
	if (!isAlive) return;
	
	blit(texture, x, y);
}

int Player::getPositionX()
{
	return x;
}

int Player::getPositionY()
{
	return y;
}

int Player::getWidth()
{
	return width;
}

int Player::getHeight()
{
	return height;
}

bool Player::getIsAlive()
{
	return isAlive;
}

bool Player::getBuffUp()
{
	return buffUp;
}

std::vector<Bullet*> Player::getBullets()
{
	return bullets;
}

void Player::doDeath()
{
	isAlive = false;
}

void Player::poweredUp()
{
	if ((app.keyboard[SDL_SCANCODE_F] || app.keyboard[SDL_SCANCODE_SPACE]) && currentReloadTime == 0)
	{
		SoundManager::playSound(sound);
		for (float i = -.20; i < .21; i += .20)
		{
			Bullet* bullet = new Bullet(x - 4 + width / 2, y - height / 2, i, -1, 10, Side::PLAYER_SIDE);
			bullets.push_back(bullet);
			getScene()->addGameObject(bullet);
		}
		//After firing, reset our reload timer
		currentReloadTime = reloadTime;
	}
}

void Player::setBuffUp(bool state)
{
	buffUp = state;
}

void Player::eraseBullet(Bullet* bullet)
{
	for (int i = 0; i < bullets.size(); i++)
	{
		if (bullets[i] == bullet)
		{
			//Cache the variable so we can delete it later
			//We can't delete it after erasing from the vector (leaked pointer)
			Bullet* bulletToErase = bullets[i];
			bullets.erase(bullets.begin() + i);
			delete bulletToErase;

			//We can't mutate (change) our vector while looping inside it
			// this might crash on the next loop iteration
			// To counter that, we only delete one bullet per frame
			// We can also avoid lag this way
			break;

		}
	}
}

