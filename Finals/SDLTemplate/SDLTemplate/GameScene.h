#pragma once
#include "Scene.h"
#include "GameObject.h"
#include "Player.h"
#include "Enemy.h"
#include "PowerUp.h"
#include "Boss.h"
#include <vector>
#include "text.h"

class GameScene : public Scene
{
public:
	GameScene();
	~GameScene();
	void start();
	void draw();
	void update();
private:
	Player* player;
	Enemy* enemy;
	PowerUp* powerup;
	Boss* boss;

	// Enemy spawning logic
	bool bossSpawned;
	float spawnTime;
	float currentSpawnTimer;
	float buffTime;
	float currentBuffTime;
	float spawnBuffTime;
	float currentSpawnBuffTime;
	float bossCurrentChangeDirectionTime;
	float direction;
	std::vector<Enemy*> spawnedEnemies;
	std::vector<Boss*> spawnedBoss;

	void doSpawnLogic();
	void doCollisionLogic();
	void enemiesBoundaryCheck();
	void spawn();
	void despawnEnemy(Enemy* enemy);
	void buffTimer();
	void spawnBuff();
	void spawnBoss();
	void despawnEnemies();
	void despawnBoss(Boss* boss);

	int points;
};

