#include "GameScene.h"

GameScene::GameScene()
{
	// Register and add game objects on constructor
	player = new Player();
	this->addGameObject(player);
	
	points = 0;
}

GameScene::~GameScene()
{
	delete player;
}

void GameScene::start()
{
	Scene::start();
	// Initialize any scene logic here

	initFonts();
	currentSpawnTimer = 300;
	spawnTime = 180; // Spawn time of 3 seconds
	currentBuffTime = 600;
	buffTime = 600;
	spawnBuffTime = 900;
	currentSpawnBuffTime = 900;
	bossSpawned = false;
	
	for (int i = 0; i < 5; i++)
	{
		spawn();
	}

}

void GameScene::draw()
{
	Scene::draw();

	drawText(110, 20, 255, 255, 255, TEXT_CENTER, "POINTS: %03d", points);

	if (player->getIsAlive() == false)
	{
		drawText(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, 255, 255, 255, TEXT_CENTER, "GAME OVER!");
	}

	else if (player->getIsAlive() == true && points >= 150)
	{
		drawText(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, 255, 255, 255, TEXT_CENTER, "YOU WON THE GAME! THANKS FOR PLAYING!");
	}

}

void GameScene::update()
{
	Scene::update();
	
	if (points < 150)
	{
		doCollisionLogic();
	}
	
	enemiesBoundaryCheck();
	spawnBuff();
	
	//Checks if player has buff
	if (player->getBuffUp() == true)
	{
		buffTimer();
	}
	
	if (points < 100 && player->getIsAlive())
	{
		doSpawnLogic();
	}
	if (points >= 100 && player->getIsAlive())
	{
		despawnEnemies();
		if (!bossSpawned)
		{
			spawnBoss();
			bossSpawned = true;
		}
	}
}

void GameScene::doSpawnLogic()
{
	if (currentSpawnTimer > 0)
		currentSpawnTimer--;
	if (currentSpawnTimer <= 0)
	{
		for (int i = 0; i < 5; i++)
		{
			spawn();
		}
		currentSpawnTimer = spawnTime;
	}
}

void GameScene::doCollisionLogic()
{
	//Check for collision
	for (int i = 0; i < objects.size(); i++)
	{
		//Cast to powerUp
		PowerUp* powerUp = dynamic_cast<PowerUp*>(objects[i]);

		// Check if cast was successfull
		if (powerUp != NULL)
		{
			int collision = checkCollision(
				player->getPositionX(), player->getPositionY(), player->getWidth(), player->getHeight(),
				powerUp->getPositionX(), powerUp->getPositionY(), powerUp->getWidth(), powerUp->getHeight()
			);
			if (collision == 1)
			{
				player->setBuffUp(true);
				currentBuffTime = buffTime;
				delete powerUp;
				break;
			}
		}

		// Cast to bullet
		Bullet* bullet = dynamic_cast<Bullet*>(objects[i]);

		// Check if the cast was successful (i.e. objects[i]) is a Bullet)
		if (bullet != NULL)
		{
			// If the bullet is the enemy side, check against the player
			if (bullet->getSide() == Side::ENEMY_SIDE)
			{
				int collision = checkCollision(
					player->getPositionX(), player->getPositionY(), player->getWidth(), player->getHeight(),
					bullet->getPositionX(), bullet->getPositionY(), bullet->getWidth(), bullet->getHeight()
				);

				if (collision == 1)
				{
					player->doDeath();
					break;
				}
			}

			// If the bullet is the player side, check against ALL enemies
			else if (bullet->getSide() == Side::PLAYER_SIDE)
			{
				
				for (int i = 0; i < spawnedBoss.size(); i++)
				{
					Boss* currentBoss = spawnedBoss[i];

					int collision = checkCollision(
						currentBoss->getPositionX(), currentBoss->getPositionY(), currentBoss->getWidth(), currentBoss->getHeight(),
						bullet->getPositionX(), bullet->getPositionY(), bullet->getWidth(), bullet->getHeight()
					);
					if (collision == 1)
					{
						currentBoss->damageBoss(1);
						std::cout << currentBoss->getHealth() << "\n";
						//Deletes the bullet in the player's bullets vector
						for (int i = 0; i < player->getBullets().size(); i++)
						{
							player->eraseBullet(bullet);
						}
						if (currentBoss->getHealth() <= 0)
						{
							std::cout << "boss killed";
							points += 50;
							delete currentBoss;
							break;
						}
						break;
					}
				
				}
				

				for (int i = 0; i < spawnedEnemies.size(); i++)
				{
					Enemy* currentEnemy = spawnedEnemies[i];

					int collision = checkCollision(
						currentEnemy->getPositionX(), currentEnemy->getPositionY(), currentEnemy->getWidth(), currentEnemy->getHeight(),
						bullet->getPositionX(), bullet->getPositionY(), bullet->getWidth(), bullet->getHeight()
					);

					if (collision == 1)
					{
						for (int i = 0; i < player->getBullets().size(); i++)
						{
							player->eraseBullet(bullet);
						}
						despawnEnemy(currentEnemy);
						//Increment points
						points++;
						//IMPORTANT: only despawn one at a time
						// otherwise we might crash due to looping while deleting
						break;
					}
				}

			}
		}
	}
}

void GameScene::enemiesBoundaryCheck()
{
	for (int i = 0; i < spawnedEnemies.size(); i++)
	{
		Enemy* currentEnemy = spawnedEnemies[i];
		if (currentEnemy->getPositionY() > 900)
		{
			despawnEnemy(currentEnemy);
		}
	}
}
void GameScene::despawnEnemies()
{
	for (int i = 0; i < spawnedEnemies.size(); i++)
	{
		Enemy* currentEnemy = spawnedEnemies[i];
		despawnEnemy(currentEnemy);
	}
}

void GameScene::spawn()
{
	
	Enemy* enemy = new Enemy();
	this->addGameObject(enemy);
	enemy->setPlayerTarget(player);

	enemy->setPosition(0 + (rand() % 600), enemy->getPositionY());
	spawnedEnemies.push_back(enemy);	
}



void GameScene::despawnEnemy(Enemy* enemy)
{
	int index = -1;
	for (int i = 0; i < spawnedEnemies.size(); i++)
	{
		// If the pointer matches
		if (enemy == spawnedEnemies[i])
		{
			index = i;
			break;
		}
	}

	// if any matche is found
	if (index != -1)
	{
		std::cout << "Enemy deleted" << "\n";
		spawnedEnemies.erase(spawnedEnemies.begin() + index);
		delete enemy;
	}
}

void GameScene::despawnBoss(Boss* boss)
{
	int index = -1;
	for (int i = 0; i < spawnedBoss.size(); i++)
	{
		// If the pointer matches
		if (boss == spawnedBoss[i])
		{
			index = i;
			break;
		}
	}

	// if any matche is found
	if (index != -1)
	{
		std::cout << "Boss deleted" << "\n";
		spawnedBoss.erase(spawnedBoss.begin() + index);
		delete boss;
	}
}

void GameScene::buffTimer()
{
	if (currentBuffTime > 0)
		currentBuffTime--;
	if (currentBuffTime <= 0)
	{
		player->setBuffUp(false);
		currentBuffTime = buffTime;
	}
}

void GameScene::spawnBuff()
{
	if (currentSpawnBuffTime > 0)
		currentSpawnBuffTime--;
	if (currentSpawnBuffTime <= 0)
	{
		PowerUp* powerUp = new PowerUp();
		this->addGameObject(powerUp);
		powerUp->setPosition(0 + (rand() % 600), powerUp->getPositionY());
		currentSpawnBuffTime = spawnBuffTime;
	}

}

void GameScene::spawnBoss()
{
	//Test if boss spawns 
	Boss* boss = new Boss();
	this->addGameObject(boss);
	boss->setPlayerTarget(player);
	boss->setPosition(0 + (rand() % 600), boss->getPositionY());
	spawnedBoss.push_back(boss);

}
