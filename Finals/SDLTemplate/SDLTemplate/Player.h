#pragma once
#include "GameObject.h"
#include "common.h"
#include "draw.h"
#include "SoundManager.h"
#include "Bullet.h"
#include <vector>

class Player : public GameObject
{
public:
    ~Player();
    void start();
    void update();
    void draw();

    int getPositionX();
    int getPositionY();
    int getWidth();
    int getHeight();
    bool getIsAlive();
    bool getBuffUp();
    std::vector<Bullet*> getBullets();
    
    void doDeath();
    void poweredUp();
    void setBuffUp(bool state);
    void eraseBullet(Bullet* bullet);

private:
    SDL_Texture* texture;
    Mix_Chunk* sound;
    int x;
    int y;
    int width;
    int height;
    int speed;
    float reloadTime;
    float currentReloadTime;
    std::vector<Bullet*> bullets;
    bool isAlive;
    bool buffUp;
};
