#pragma once
#include "GameObject.h"
#include "common.h"
#include "draw.h"

class PowerUp : public GameObject
{
public: 
	PowerUp();
	void start();
	void update();
	void draw();
	void setPosition(int xPos, int yPos);

	int getPositionX();
	int getPositionY();
	int getWidth();
	int getHeight();
private: 
	SDL_Texture* texture;
	int x;
	int y; 
	float directionX;
	float directionY;
	int width;
	int height;
	int speed;
};

