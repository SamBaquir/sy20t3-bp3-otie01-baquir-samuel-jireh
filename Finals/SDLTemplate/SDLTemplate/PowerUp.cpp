#include "PowerUp.h"
#include "Scene.h"

PowerUp::PowerUp()
{

}

void PowerUp::start()
{
	//Load Texture
	texture = loadTexture("gfx/points.png");

	//Initialize to avoid garbage values
	directionX = -1;
	directionY = 1;
	width = 0;
	height = 0;
	speed = 2;

	// Query the texture to set our width and height
	SDL_QueryTexture(texture, NULL, NULL, &width, &height);
}

void PowerUp::update()
{
	// Move
	//x += directionX * speed;
	y += directionY * speed;
}

void PowerUp::draw()
{
	blit(texture, x, y);
}

void PowerUp::setPosition(int xPos, int yPos)
{
	this->x = xPos;
	this->y = yPos;
}

int PowerUp::getPositionX()
{
	return x;
}

int PowerUp::getPositionY()
{
	return y;
}

int PowerUp::getWidth()
{
	return width;
}

int PowerUp::getHeight()
{
	return height;
}
