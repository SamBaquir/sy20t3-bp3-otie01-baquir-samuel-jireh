#include "Boss.h"
#include "Scene.h"

Boss::Boss()
{
}

Boss::~Boss()
{
}

void Boss::start()
{
	//Loads texture
	texture = loadTexture("gfx/enemy.png");

	directionX = -1;
	width = 0;
	height = 0;
	speed = 1;
	reloadTime = 26; // Reload time of 120 frames, or 0.13 seconds
	health = 50;

	// Query the texture to set our width and height
	SDL_QueryTexture(texture, NULL, NULL, &width, &height);

	sound = SoundManager::loadSound("sound/334227__jradcoolness__laser.ogg");
	sound->volume = 50;
}

void Boss::update()
{
	//Move left or right
	x += directionX * speed;

	//Switch directions immediately if collides with edge
	if (x < 0 || x > SCREEN_WIDTH - width)
	{
		directionX = -directionX;
	}
	//Decrement the enemy's reload timer
	if (currentReloadTime > 0)
		currentReloadTime--;

	// Only fire when our reload timer is ready
	if (currentReloadTime == 0)
	{
		float dx = -1;
		float dy = 0;

		calcSlope(playerTarget->getPositionX(), playerTarget->getPositionY(), x, y, &dx, &dy);

		SoundManager::playSound(sound);
		for (float i = -2; i < 3; i+=.25)
		{
			if (i == 0) continue;
			Bullet* bullet = new Bullet(x - 3 + width / 2, y + height, i, dy, 4, Side::ENEMY_SIDE);
			bullets.push_back(bullet);
			getScene()->addGameObject(bullet);
		}

		Bullet* homingBullet = new Bullet(x - 3 + width / 2, y + height, dx, dy, 3, Side::ENEMY_SIDE);
		bullets.push_back(homingBullet);
		getScene()->addGameObject(homingBullet);
		
		//After firing, reset our reload timer
		currentReloadTime = reloadTime;
	}




	// Memory manage our bullets. When they go off screen, delete them
	for (int i = 0; i < bullets.size(); i++)
	{
		if (bullets[i]->getPositionY() >= 900 || bullets[i]->getPositionY() <= 0 || bullets[i]->getPositionX() >= SCREEN_WIDTH || bullets[i]->getPositionX() <= 0)
		{
			//Cache the variable so we can delete it later
			Bullet* bulletToErase = bullets[i];
			bullets.erase(bullets.begin() + i);
			delete bulletToErase;

			//std::cout << "Bullet destroyed!" << "\n";
			break;
		}
	}
}

void Boss::draw()
{
	blit(texture, x, y);
}

void Boss::setPlayerTarget(Player* player)
{
	playerTarget = player;
}

void Boss::setPosition(int xPos, int yPos)
{
	this->x = xPos;
	this->y = yPos;
}

void Boss::damageBoss(int damage)
{
	health -= damage;
}

int Boss::getPositionX()
{
	return x;
}

int Boss::getPositionY()
{
	return y;
}

int Boss::getWidth()
{
	return width;
}

int Boss::getHeight()
{
	return height;
}

int Boss::getHealth()
{
	return health;
}

